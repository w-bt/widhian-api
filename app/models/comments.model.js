module.exports = (sequelize, DataTypes) => {
    return sequelize.define("comment", {
        name: {
            type: DataTypes.STRING
        },
        text: {
            type: DataTypes.STRING
        }
    });
};