const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
    host: dbConfig.HOST,
    dialect: dbConfig.dialect,
    operatorsAliases: 0,

    pool: {
        max: dbConfig.pool.max,
        min: dbConfig.pool.min,
        acquire: dbConfig.pool.acquire,
        idle: dbConfig.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.article = require("./articles.model.js")(sequelize, Sequelize);
db.comment = require("./comments.model.js")(sequelize, Sequelize);
db.category = require("./categories.model.js")(sequelize, Sequelize);
db.user = require("./users.model.js")(sequelize, Sequelize);

db.article.hasMany(db.comment, { as: "comments" })
db.comment.belongsTo(db.article, {
    foreignKey: "articleId",
    as: "article",
});

db.category.hasMany(db.article, { as: "articles" })
db.article.belongsTo(db.category, {
    foreignKey: "categoryId",
    as: "category",
});

module.exports = db;