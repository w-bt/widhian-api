module.exports = (sequelize, Sequelize) => {
    return sequelize.define("article", {
        title: {
            type: Sequelize.STRING
        },
        slug: {
            type: Sequelize.STRING,
            unique: true
        },
        description: {
            type: Sequelize.TEXT
        },
        published: {
            type: Sequelize.BOOLEAN
        }
    }, {
        indexes: [
            {
                unique: true,
                fields: ['slug']
            },
            {
                fields: ['createdAt']
            },
            {
                fields: ['updatedAt']
            },
            {
                fields: ['createdAt', 'slug']
            },
        ]
    });
};