const db = require("../models");
const Comment = db.comment;

// create comment
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name || !req.body.text) {
        res.status(400).send({
            message: "Name or content can not be empty!"
        });
        return;
    }

    // Create a Comment
    const comment = {
        name: req.body.name,
        text: req.body.text,
        tutorialId: req.body.tutorialId
    };

    Comment.create(comment)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the comment."
            });
        });
};