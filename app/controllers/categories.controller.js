const db = require("../models");
const Category = db.category;

// create category
exports.create = (req, res) => {
    // Validate request
    if (!req.body.name) {
        res.status(400).send({
            message: "Name can not be empty!"
        });
        return;
    }

    const slug = req.body.slug ? req.body.slug : req.body.name.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');

    // Create a Category
    const category = {
        name: req.body.name,
        slug: slug
    };

    Category.create(category)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the category."
            });
        });
};

// Retrieve all Categories from the database.
exports.findAll = (req, res) => {
    const name = req.query.name;
    let condition = name ? {name: {[Op.iLike]: `%${name}%`}} : null;

    Category.findAll({ where: condition, include: ["articles"], order: [['id', 'ASC']] })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving categories."
            });
        });
};

// Delete a Category with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Category.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num === 1 || (Array.isArray(num) && num.length == 1 && num[0] === 1)) {
                res.send({
                    message: "Category was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Category with id=${id}. Maybe Category was not found!`
                });
            }
        })
        .catch(() => {
            res.status(500).send({
                message: "Could not delete Category with id=" + id
            });
        });
};

// Update a Category by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    const slug = req.body.slug ? req.body.slug : req.body.name.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');

    // Create a Category
    const category = {
        name: req.body.name,
        slug: slug
    };

    Category.update(category, {
        where: { id: id }
    })
        .then(num => {
            if (num === 1 || (Array.isArray(num) && num.length === 1 && num[0] === 1)) {
                res.send({
                    message: "Category was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Category with id=${id}. Maybe Category was not found or req.body is empty!`
                });
            }
        })
        .catch(() => {
            res.status(500).send({
                message: "Error updating Category with id=" + id
            });
        });
};