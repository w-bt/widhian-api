const db = require("../models");
const Article = db.article;
const Category = db.category;
const Op = db.Sequelize.Op;

// Create and Save a new Article
exports.create = (req, res) => {
    // Validate request
    if (!req.body.title) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
        return;
    }

    if (!req.body.categoryId) {
        res.status(400).send({
            message: "Category can not be empty!"
        });
        return
    } else {
        Category.findByPk(req.body.categoryId)
            .catch((err) => {
                console.log(">> Error while finding category: ", err);
                res.status(400).send({
                    message: "Category not found!"
                });
            });
    }

    const slug = req.body.slug ? req.body.slug : req.body.title.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'');

    // Create a Article
    const article = {
        title: req.body.title,
        slug:  slug,
        description: req.body.description,
        categoryId: req.body.categoryId,
        published: req.body.published ? req.body.published : false
    };

    // Save Article in the database
    Article.create(article)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Article."
            });
        });
};

// Retrieve all Articles from the database.
exports.findAll = (req, res) => {
    const title = req.query.title;
    let condition = title ? {title: {[Op.iLike]: `%${title}%`}} : null;

    Article.findAll({ where: condition, include: ["comments", "category"], order: [['createdAt', 'DESC']] })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving articles."
            });
        });
};

// Find all published Articles
exports.findAllPublished = (req, res) => {
    const limit = req.query.limit ? req.query.limit : 10;
    const offset = req.query.offset ? req.query.offset : 0;

    Article.findAll({ where: { published: true }, include: ["comments", "category"], order: [['createdAt', 'DESC']], limit: limit, offset: offset })
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving articles."
            });
        });
};

// Count all published Articles
exports.countAllPublished = (req, res) => {
    Article.count({ where: { published: true }})
        .then(data => {
            res.send({total: data});
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while counting articles."
            });
        });
};

// Find a single Article with an slug
exports.findOneWithSlug = (req, res) => {
    const year = req.params.year;
    const month = req.params.month;
    const date = req.params.date;
    const slug = req.params.slug;
    const lowerBound = new Date(year + '-' + month + '-' + date + ' 00:00:00.000 +07:00');
    const upperBound = new Date(year + '-' + month + '-' + date + ' 23:59:59.000 +07:00');
    let condition = {[Op.and]: [{ slug: slug }, {createdAt: {[Op.between]: [lowerBound, upperBound]}}]}

    Article.findOne({ where: condition, include: ["comments", "category"] })
        .then(data => {
            res.send(data);
        })
        .catch(() => {
            res.status(500).send({
                message: "Error retrieving Article with id=" + id
            });
        });
};

// Find a single Article with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

    Article.findByPk(id, { include: ["comments", "category"] })
        .then(data => {
            res.send(data);
        })
        .catch(() => {
            res.status(500).send({
                message: "Error retrieving Article with id=" + id
            });
        });
};

// Update a Article by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

    Article.update(req.body, {
        where: { id: id }
    })
        .then(num => {
            if (num === 1 || (Array.isArray(num) && num.length === 1 && num[0] === 1)) {
                res.send({
                    message: "Article was updated successfully."
                });
            } else {
                res.send({
                    message: `Cannot update Article with id=${id}. Maybe Article was not found or req.body is empty!`
                });
            }
        })
        .catch(() => {
            res.status(500).send({
                message: "Error updating Article with id=" + id
            });
        });
};

// Delete a Article with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Article.destroy({
        where: { id: id }
    })
        .then(num => {
            if (num === 1 || (Array.isArray(num) && num.length === 1 && num[0] === 1)) {
                res.send({
                    message: "Article was deleted successfully!"
                });
            } else {
                res.send({
                    message: `Cannot delete Article with id=${id}. Maybe Article was not found!`
                });
            }
        })
        .catch(() => {
            res.status(500).send({
                message: "Could not delete Article with id=" + id
            });
        });
};

// Delete all Articles from the database.
exports.deleteAll = (req, res) => {
    Article.destroy({
        where: {},
        truncate: false
    })
        .then(nums => {
            res.send({ message: `${nums} Articles were deleted successfully!` });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all articles."
            });
        });
};