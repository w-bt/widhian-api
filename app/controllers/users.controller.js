const db = require("../models");
const User = db.user;
const CryptoJS = require('crypto-js');
const { v4: uuidv4 } = require('uuid');

// Create and Save a new User
exports.create = (req, res) => {
    // Validate request
    if (!req.body.email) {
        res.status(400).send({
            message: "Email can not be empty!"
        });
        return;
    }

    if (!req.body.password) {
        res.status(400).send({
            message: "Password can not be empty!"
        });
        return;
    }

    // Create a User
    const user = {
        email: req.body.email,
        password: CryptoJS.MD5(req.body.password).toString()
    };

    // Save User in the database
    User.create(user)
        .then(data => {
            res.send({ "message": "success" });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            });
        });
};

// Login
exports.login = (req, res) => {
    const email = req.body.email;
    const password = CryptoJS.MD5(req.body.password).toString();

    User.findAll({
        where: {
            email: email,
            password: password
        }
    }).then(data => {
        if (data.length == 0 || !data) {
            res.status(401).send({
                message: "user not found",
            });
            return;
        }

        res.send({
            message: "success",
            user: data[0].email,
            token: uuidv4()
        });
    }).catch(err => {
        res.status(404).send({
            message: err.message || "user not found",
        });
    });
};