module.exports = app => {
    const articles = require("../controllers/articles.controller.js");
    const comments = require("../controllers/comments.controller.js");
    const categories = require("../controllers/categories.controller.js");
    const users = require("../controllers/users.controller.js");

    let router = require("express").Router();

    // Create a new Article
    router.post("/articles/", articles.create);

    // Retrieve all Articles
    router.get("/articles/", articles.findAll);

    // Retrieve all published Articles
    router.get("/articles/published", articles.findAllPublished);

    // Retrieve total all published Articles
    router.get("/articles/published/count", articles.countAllPublished);

    // Retrieve a single Article with id
    router.get("/articles/:id", articles.findOne);

    // Retrieve a single Article with date and path
    router.get("/articles/:year/:month/:date/:slug", articles.findOneWithSlug);

    // Update a Article with id
    router.put("/articles/:id", articles.update);

    // Delete a Article with id
    router.delete("/articles/:id", articles.delete);

    // Create a new Article
    router.delete("/articles/", articles.deleteAll);

    // Create a new comment
    router.post("/comments", comments.create);

    // Create a new Category
    router.post("/categories", categories.create);

    // Retrieve all Categories
    router.get("/categories", categories.findAll);

    // Delete a Category with id
    router.delete("/categories/:id", categories.delete);

    // Update a Category with id
    router.put("/categories/:id", categories.update);

    // Create a new user
    router.post("/users", users.create);

    // user login
    router.post("/login", users.login);

    app.use('/api', router);
};